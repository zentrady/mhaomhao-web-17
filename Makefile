DOCKER ?= docker
# DOCKER_COMPOSE ?= docker-compose
# KUBECTL ?= kubectl
# GCLOUND ?= gcloud

STAGING_VERSION ?= 1.0.0
# PRODUCTION_VERSION ?= v1.0.0

#----------staging cmd----------
build-staging:
	$(DOCKER) build -t mhaomhao-frontend:$(STAGING_VERSION) .

push-img-staging:
	$(GCLOUND) docker -- push mhaomhao-frontend:$(STAGING_VERSION)
# --network="mhaomhao-backend_mm_network"
run-staging:
	$(DOCKER) run --network="mhaomhao-backend_mm_network" --name mhaomhao-frontend -p 3000:3000 -d mhaomhao-frontend:$(STAGING_VERSION)

stop-staging:
	$(DOCKER) stop mhaomhao-frontend

rm-staging:
	$(DOCKER) rm mhaomhao-frontend

rm-image:
	$(DOCKER) image rm mhaomhao-frontend:$(STAGING_VERSION)

deploy:
	make stop-staging && make rm-staging && make build-staging && make run-staging
