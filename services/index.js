import HttpRequestProvider from './http_request'
import AuthAPI from './authApi'
import AuctionAPI from './auctionApi'
import PaymentAPI from './paymentApi'
import ArticleAPI from './articleApi'
import EmploymentAPI from './employmentApi'

export const HttpRequest = new HttpRequestProvider()
export const AuthService = new AuthAPI()
export const AuctionService = new AuctionAPI()
export const PaymentService = new PaymentAPI()
export const ArticleService = new ArticleAPI()
export const EmploymentService = new EmploymentAPI()
